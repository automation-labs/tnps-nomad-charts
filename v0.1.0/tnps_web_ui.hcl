job "tnps_web_ui_test" {

  datacenters = ["dc1"]
  namespace   = "tnps"

  group "tnps_web_ui_test" {
    network {
      mode = "bridge"
      port "3000" {
        to     = 3000
        static = 3001
      }
    }

    service {
      name = "tnps-web-ui-test"
      port = "3000"
      tags = ["traefik.enable=true", "traefik.http.routers.web-ui-test.rule=Host(`tnps-web-ui-test.nomad.atklik.xyz`)", "traefik.http.routers.web-ui-test.service=tnps-web-ui-test"]
    }

    task "tnps_web_ui_test" {
      driver = "docker"

      config {
        image = "registry.digitalocean.com/labs-registry/tnps/web-ui:test-3"
        auth {
          username = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"
          password = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"
        }

        ports = ["3000"]
      }

      resources {
        cpu    = 1024
        memory = 1024
      }
    }
  }
}


