job [[ template "job_name" . ]] {
  [[ template "region" . ]]
  datacenters = [[ var "datacenters" . | toStringList ]]
  namespace = [[ var "namespace" . | quote ]]
  type = "service"

  [[- if var "prod_env" . ]]
    constraint {
        attribute = [[ var "constraint.attribute" . | quote ]]
        value     = [[ var "constraint.value" . | quote ]]
    }
    [[- end ]]

    group "tnps_user_service" {
        [[- if var "static_count" . ]]
        count = [[  var "tnps_user_service_count" . ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if var "expose_stanza" . ]]
            port "[[  var "tnps_user_service_port" . ]]" {
                to = [[  var "tnps_user_service_port" . ]]
                static = [[  var "tnps_user_service_port" . ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ var "service_name_tnps_user_service" . ]]"
            port = "[[ var "tnps_user_service_port" . ]]"
            tags = [[ var "tags" . | toStringList ]]
            connect {
                sidecar_service{
                }
            }

        [[- if var "enable_autocale" . ]]
        scaling {
            enabled = [[ var "scaling.enabled" . ]]
            min     = [[ var "scaling.min" . ]]
            max     = [[ var "scaling.max" . ]]
            policy {
                cooldown            = "[[ var "scaling.cooldown" . ]]"
                evaluation_interval = "[[ var "scaling.evaluation_interval" . ]]"
                check "cpu" {
                source = "[[ var "scaling_cpu.source" . ]]"
                query  = "[[ var "scaling_cpu.query" . ]]"
                strategy "target-value" {
                    target = [[ var "scaling_cpu.target" . ]]
                }
                }
                check "memory" {
                source = "[[ var "scaling_memory.source" . ]]"
                query  = "[[ var "scaling_memory.query" . ]]"
                strategy "target-value" {
                    target = [[ var "scaling_memory.target" . ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "tnps_user_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ var "secret" .]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ var "image" . ]]:[[ var "image_tag" . ]]"
                [[- if var "auth_image" . ]]
                auth {
                     [[- range $var := var "docker_auth" . ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if var "cores_param" . ]]
                cores  = [[ var "resources.cores" . ]]
                [[- else ]]
                cpu = [[ var "resources.cpu" . ]]
                [[- end ]]
                memory = [[ var "resources.memory" . ]]
            }
        }
    }
}

