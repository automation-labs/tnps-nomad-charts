variable "job_name" {
  type        = string
  // If "", the pack name will be used
  default = ""
}

variable "region" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement"
  type        = list(string)
  default     = ["dc1"]
}

variable "namespace" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = "tnps"
}

variable "constraint" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        })
        default = {
        attribute = "$${meta.node}"
        value     = "tnps"
        }
}

variable "constraint_tsel" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        operator    = string
        })
        default = {
        attribute = "nomadworkertbspapp30,nomadworkertbspapp29,nomadworkertbspapp28,nomadworkertbspapp26"
        value     = "$${attr.unique.hostname}"
        operator  = "set_contains"
        }
}

variable "expose_stanza"{
  description = "enable expose stanza" 
  type = bool
  default = true
}

variable "cores_param"{
  description = "cores param" 
  type = bool
  default = false
}

variable "prod_env"{
  description = "enable constrains" 
  type = bool
  default = false
}

variable "enable_autoscale"{
  description = "enable autoscale" 
  type = bool
  default = false
}

variable "static_count"{
  description = "enable count static" 
  type = bool
  default = true
}

variable "kafka_ui_count" {
  description = "kafka-ui static count, this count use for kafka-ui replication"
  type = number
  default = 1
}

variable "scaling" {
  description = "autoscaler"
  type = object({
    enabled = bool
    min = number
    max = number
    cooldown = string
    evaluation_interval = string
  })
  default = {
    enabled = false,
    min = 1,
    max = 5,
    cooldown = "20s",
    evaluation_interval = "10s"
  }
}

variable "scaling_cpu" {
  description = "scaling_cpu"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_cpu-allocated",
    target = 50
  }
}

variable "scaling_memory" {
  description = "scaling_memory"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_memory-allocated",
    target = 50
  }
}

variable "service_name_kafka_ui" {
  description = "service name kafka-ui"
  type        = string
  default     = "kafka-ui"
}

variable "kafka_ui_port" {
  description = "kafka-ui port, this port use for kafka-ui connection"
  type = number
  default = 8080
}

variable "tags" {
  description = "A list of tags traefik"
  type        = list(string)
  default     = [
        "traefik.enable=true",
        "traefik.http.routers.kafka-ui.rule=Host(`kafka-ui.nomad.atklik.xyz`)",
        "traefik.http.routers.kafka-ui.service=kafka-ui"
      ]
}

variable "kafka_ui_image" {
  description = "Docker image for kafka-ui"
  type        = string 
  default     = "provectuslabs/kafka-ui"
}

variable "kafka_ui_image_tag" {
  description = "Docker image tags for kafka-ui"
  type        = string
  default     = "latest"
}

variable "auth_image" {
  description = "enable auth image" 
  type = bool
  default = false
}

variable "kafka_ui_auth" {
  description = "Docker image for kafka-ui"
  type = list(object({
    key   = string
    value = string
  }))
  default = [
    {key = "username", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"},
    {key = "password", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"}
  ]
}

variable "secret" {
  description = "environment secret kafka-ui"
  type = string
  default = <<EOF
    KAFKA_CLUSTERS_0_NAME= kliklabs-cluster
    KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS= 10.101.120.25:9092,10.101.120.26:9092,10.101.120.27:9092
    DYNAMIC_CONFIG_ENABLED= true
    AUTH_TYPE= "LOGIN_FORM"
    SPRING_SECURITY_USER_NAME= admin
    SPRING_SECURITY_USER_PASSWORD= Password@KLIK123
    EOF
}

variable "certificate_rootca" {
  description = ".crt"
  type = string
  default = <<EOF
EOF
}

variable "resources" {
  description = "The resource to assign to the kafka-ui"
  type = object({
    cores  = number
    cpu    = number
    memory = number
  })
  default = {
    cores  = 1,
    cpu    = 300,
    memory = 500
  }
}
