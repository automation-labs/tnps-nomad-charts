job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ var "datacenters" . | toStringList ]]
    namespace = [[ var "namespace" . | quote ]]

    [[- if var "prod_env" . ]]
    constraint {
        attribute = [[ var "constraint.attribute" . | quote ]]
        value     = [[ var "constraint.value" . | quote ]]
    }
    [[- end ]]

    group "kafka_ui" {
        [[- if var "static_count" . ]]
        count = [[  var "kafka_ui_count" . ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if var "expose_stanza" . ]]
            port "[[  var "kafka_ui_port" . ]]" {
                to = [[  var "kafka_ui_port" . ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ var "service_name_kafka_ui" . ]]"
            port = "[[ var "kafka_ui_port" . ]]"
            tags = [[ var "tags" . | toStringList ]]
            connect {
                sidecar_service{
                }
                sidecar_task {
                    resources {
                        cpu = 500
                        memory = 1024
                    }
                }
            }

        [[- if var "enable_autocale" . ]]
        scaling {
            enabled = [[ var "scaling.enabled" . ]]
            min     = [[ var "scaling.min" . ]]
            max     = [[ var "scaling.max" . ]]
            policy {
                cooldown            = "[[ var "scaling.cooldown" . ]]"
                evaluation_interval = "[[ var "scaling.evaluation_interval" . ]]"
                check "cpu" {
                source = "[[ var "scaling_cpu.source" . ]]"
                query  = "[[ var "scaling_cpu.query" . ]]"
                strategy "target-value" {
                    target = [[ var "scaling_cpu.target" . ]]
                }
                }
                check "memory" {
                source = "[[ var "scaling_memory.source" . ]]"
                query  = "[[ var "scaling_memory.query" . ]]"
                strategy "target-value" {
                    target = [[ var "scaling_memory.target" . ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "kafka_ui" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ var "secret" . ]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ var "kafka_ui_image" . ]]:[[ var "kafka_ui_image_tag" . ]]"
                [[- if var "auth_image" . ]]
                auth {
                     [[- range $var := var "kafka_ui_auth" . ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if var "cores_param" . ]]
                cores  = [[ var "resources.cores" . ]]
                [[- else ]]
                cpu = [[ var "resources.cpu" . ]]
                [[- end ]]
                memory = [[ var "resources.memory" . ]]
            }
        }
    }
}
