variable "job_name" {
  # If "", the pack name will be used
  description = "The name to use as the job name which overrides using the pack name"
  type        = string
  default     = ""
}

variable "region" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement"
  type        = list(string)
  default     = ["dc1"]
}

variable "namespace" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = "tnps"
}

variable "constraint" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        })
        default = {
        attribute = "$${meta.node}"
        value     = "tnps"
        }
}

variable "constraint_tsel" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        operator    = string
        })
        default = {
        attribute = "nomadworkertbspapp30,nomadworkertbspapp29,nomadworkertbspapp28,nomadworkertbspapp26"
        value     = "$${attr.unique.hostname}"
        operator  = "set_contains"
        }
}

variable "expose_stanza"{
  description = "enable expose stanza" 
  type = bool
  default = true
}

variable "cores_param"{
  description = "cores param" 
  type = bool
  default = false
}

variable "prod_env"{
  description = "enable contains" 
  type = bool
  default = false
}

variable "enable_autoscale"{
  description = "enable autoscale" 
  type = bool
  default = false
}

variable "static_count"{
  description = "enable count static" 
  type = bool
  default = false
}

variable "tnps_channel_service_count" {
  description = "tnps-channel-service static count, this count use for tnps-channel-service replication"
  type = number
  default = 3
}

variable "scaling" {
  description = "autoscaler"
  type = object({
    enabled = bool
    min = number
    max = number
    cooldown = string
    evaluation_interval = string
  })
  default = {
    enabled = true,
    min = 1,
    max = 5,
    cooldown = "20s",
    evaluation_interval = "10s"
  }
}

variable "scaling_cpu" {
  description = "scaling_cpu"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_cpu-allocated",
    target = 50
  }
}

variable "scaling_memory" {
  description = "scaling_memory"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_memory-allocated",
    target = 50
  }
}

variable "service_name_tnps_channel_service" {
  description = "service name tnps-channel-service"
  type        = string
  default     = "tnps-channel-service"
}

variable "tnps_channel_service_port" {
  description = "tnps-channel-service port, this port use for tnps-channel-service connection"
  type = number
  default = 4300
}

variable "tags" {
  description = "A list of tags traefik"
  type        = list(string)
  default     = [
        "traefik.enable=true",
        "traefik.http.routers.channel-service.rule=Host(`tnps-channel-service.nomad.atklik.xyz`)",
        "traefik.http.routers.channel-service.service=tnps-channel-service"
      ]
}

variable "image" {
  description = "Docker image for tnps-channel-service"
  type        = string 
  default     = "registry.digitalocean.com/labs-registry/tnps/channel-service"
}

variable "image_tag" {
  description = "Docker image tags for tnps-channel-service"
  type        = string
  default     = "alpha-26"
}

variable "auth_image" {
  description = "enable auth image" 
  type = bool
  default = true
}

variable "docker_auth" {
  description = "Docker image for tnps-channel-service"
  type = list(object({
    key   = string
    value = string
  }))
  default = [
    {key = "username", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"},
    {key = "password", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"}
  ]
}

variable "secret" {
  description = "environment secret tnps-channel-service"
  type = string
  default = <<EOF
    ENV= "SIT"
    APP_PORT= "4300"
    SECRET_KEY= "secret"
    MONGODB_URI= "mongodb://kliklabs:Password%40KLIK2023!@10.101.110.30:27017,10.101.110.28:27017,10.101.110.29:27017/?replicaSet=rs-nonprod&readPreference=primaryPreferred"
    DB_CHANNEL_SERVICE= "tnps_channel_service"
    MINIO_ENDPOINT= "minio-api.labs-nonprod.atklik.xyz"
    MINIO_DOWNLOAD_FILE_STORE= "files/"
    MINIO_ACCESS_KEY= "HFYeuiZchzWquDnG"
    MINIO_SECRET_KEY= "hMdrzhPM2fLAmcMp7cTdLzGHolbC1rXN"
    MINIO_BUCKET_NAME= "tnps-dev"
    MINIO_USE_SSL= "true"
    MINIO_DURATION_PATH= "24h"
    MINIO_CHANNELS_PATH= "channels/"
    MINIO_JOURNEYS_PATH= "journeys/"
    
    EOF
}

variable "certificate_rootca" {
  description = ".crt"
  type = string
  default = <<EOF
EOF
}

variable "resources" {
  description = "The resource to assign to the tnps-channel-service"
  type = object({
    cores  = number
    cpu    = number
    memory = number
  })
  default = {
    cores  = 1,
    cpu    = 300,
    memory = 500
  }
}
