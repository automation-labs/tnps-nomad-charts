job [[ template "job_name" . ]] {
  [[ template "region" . ]]
  datacenters = [[ var "datacenters" . | toStringList ]]
  namespace = [[ var "namespace" . | quote ]]
  type = "service"

  [[- if var "prod_env" . ]]
    constraint {
        attribute = [[ var "constraint.attribute" . | quote ]]
        value     = [[ var "constraint.value" . | quote ]]
    }
    [[- end ]]

    group "tnps_apigw" {
        [[- if var "static_count" . ]]
        count = [[  var "tnps_apigw_count" . ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if var "expose_stanza" . ]]
            port "[[  var "tnps_apigw_port" . ]]" {
                to = [[  var "tnps_apigw_port" . ]]
                static = 28000
            }
            [[- end ]]
        }

        service {
            name = "[[ var "service_name_tnps_apigw" . ]]"
            port = "[[ var "tnps_apigw_port" . ]]"
            tags = [[ var "tags" . | toStringList ]]
            connect {
                sidecar_service{
            proxy {
            upstreams {
              destination_name = "tnps-user-service"
              local_bind_port  = 1313
            }
            upstreams {
              destination_name = "tnps-channel-service"
              local_bind_port  = 1414
            }
            upstreams {
              destination_name = "tnps-survey-service"
              local_bind_port  = 1515
            }
            upstreams {
              destination_name = "tnps-detractor-service"
              local_bind_port  = 1616
            }
          }
        }
        }

        [[- if var "enable_autocale" . ]]
        scaling {
            enabled = [[ var "scaling.enabled" . ]]
            min     = [[ var "scaling.min" . ]]
            max     = [[ var "scaling.max" . ]]
            policy {
                cooldown            = "[[ var "scaling.cooldown" . ]]"
                evaluation_interval = "[[ var "scaling.evaluation_interval" . ]]"
                check "cpu" {
                source = "[[ var "scaling_cpu.source" . ]]"
                query  = "[[ var "scaling_cpu.query" . ]]"
                strategy "target-value" {
                    target = [[ var "scaling_cpu.target" . ]]
                }
                }
                check "memory" {
                source = "[[ var "scaling_memory.source" . ]]"
                query  = "[[ var "scaling_memory.query" . ]]"
                strategy "target-value" {
                    target = [[ var "scaling_memory.target" . ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "tnps_apigw" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ var "secret" .]]
                EOF

                destination = "/local/klikapim.json"
                env = false
            }

            config {
                image = "[[ var "image" . ]]:[[ var "image_tag" . ]]"
                mount {
                     [[- range $var := var "mount" . ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- if var "auth_image" . ]]
                auth {
                     [[- range $var := var "docker_auth" . ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if var "cores_param" . ]]
                cores  = [[ var "resources.cores" . ]]
                [[- else ]]
                cpu = [[ var "resources.cpu" . ]]
                [[- end ]]
                memory = [[ var "resources.memory" . ]]
            }
        }
    }
}

