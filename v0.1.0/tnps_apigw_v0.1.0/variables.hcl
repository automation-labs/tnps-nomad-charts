variable "job_name" {
  # If "", the pack name will be used
  description = "The name to use as the job name which overrides using the pack name"
  type        = string
  default     = ""
}

variable "region" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement"
  type        = list(string)
  default     = ["dc1"]
}

variable "namespace" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = "tnps"
}

variable "constraint" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        })
        default = {
        attribute = "$${meta.node}"
        value     = "tnps"
        }
}

variable "constraint_tsel" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        operator    = string
        })
        default = {
        attribute = "nomadworkertbspapp30,nomadworkertbspapp29,nomadworkertbspapp28,nomadworkertbspapp26"
        value     = "$${attr.unique.hostname}"
        operator  = "set_contains"
        }
}

variable "expose_stanza"{
  description = "enable expose stanza" 
  type = bool
  default = true
}

variable "cores_param"{
  description = "cores param" 
  type = bool
  default = false
}

variable "prod_env"{
  description = "enable contains" 
  type = bool
  default = false
}

variable "enable_autoscale"{
  description = "enable autoscale" 
  type = bool
  default = false
}

variable "static_count"{
  description = "enable count static" 
  type = bool
  default = false
}

variable "tnps_apigw_count" {
  description = "tnps-apigw static count, this count use for tnps-apigw replication"
  type = number
  default = 3
}

variable "scaling" {
  description = "autoscaler"
  type = object({
    enabled = bool
    min = number
    max = number
    cooldown = string
    evaluation_interval = string
  })
  default = {
    enabled = true,
    min = 1,
    max = 5,
    cooldown = "20s",
    evaluation_interval = "10s"
  }
}

variable "scaling_cpu" {
  description = "scaling_cpu"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_cpu-allocated",
    target = 50
  }
}

variable "scaling_memory" {
  description = "scaling_memory"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_memory-allocated",
    target = 50
  }
}

variable "service_name_tnps_apigw" {
  description = "service name tnps-apigw"
  type        = string
  default     = "tnps-apigw"
}

variable "tnps_apigw_port" {
  description = "tnps-apigw port, this port use for tnps-apigw connection"
  type = number
  default = 8080
}

variable "tags" {
  description = "A list of tags traefik"
  type        = list(string)
  default     = [
        "traefik.enable=true",
        "traefik.http.routers.apigw.rule=Host(`tnps-apigw.nomad.atklik.xyz`)",
        "traefik.http.routers.apigw.service=tnps-apigw"
      ]
}

variable "image" {
  description = "Docker image for tnps-apigw"
  type        = string 
  default     = "registry.digitalocean.com/labs-registry/tnps/apigw"
}

variable "image_tag" {
  description = "Docker image tags for tnps-apigw"
  type        = string
  default     = "alpha-26"
}

variable "auth_image" {
  description = "enable auth image" 
  type = bool
  default = true
}

variable "docker_auth" {
  description = "Docker image for tnps-apigw"
  type = list(object({
    key   = string
    value = string
  }))
  default = [
    {key = "username", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"},
    {key = "password", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"}
  ]
}

variable "mount" {
 description = "Docker image for klikapim"
   type = list(object({
   key = string
   value = string
 }))
 default = [
   {key = "type", value = "bind"},
   {key = "source", value = "local"},
   {key = "target", value = "/etc/klikapim"}
 ]
}

variable "secret" {
  description = "environment secret tnps-apigw"
  type = string
  default = <<EOF
    EOF
}

variable "certificate_rootca" {
  description = ".crt"
  type = string
  default = <<EOF
EOF
}

variable "resources" {
  description = "The resource to assign to the tnps-apigw"
  type = object({
    cores  = number
    cpu    = number
    memory = number
  })
  default = {
    cores  = 1,
    cpu    = 300,
    memory = 500
  }
}
