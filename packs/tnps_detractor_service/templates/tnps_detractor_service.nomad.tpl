job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .tnps_detractor_service.datacenters | toStringList ]]
    namespace = [[ .tnps_detractor_service.namespace | quote ]]

    [[- if .tnps_detractor_service.prod_env ]]
    constraint {
        attribute = [[ .tnps_detractor_service.constraint.attribute | quote ]]
        value     = [[ .tnps_detractor_service.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_detractor_service" {
        [[- if .tnps_detractor_service.static_count ]]
        count = [[  .tnps_detractor_service.tnps_detractor_service_count ]]
        [[- end ]]

        network {
            mode = "bridge"
            [[- if .tnps_detractor_service.expose_stanza ]]
            port "[[  .tnps_detractor_service.tnps_detractor_service_port ]]" {
                to = [[  .tnps_detractor_service.tnps_detractor_service_port ]]
                static = [[  .tnps_detractor_service.tnps_detractor_service_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_detractor_service.service_name_tnps_detractor_service ]]"
            port = "[[ .tnps_detractor_service.tnps_detractor_service_port ]]"
            tags = [[ .tnps_detractor_service.tags | toStringList ]]
            connect {
                sidecar_service{
                }
            }

            [[- if .tnps_detractor_service.enable_autocale ]]
            scaling {
                enabled = [[ .tnps_detractor_service.scaling.enabled ]]
                min     = [[ .tnps_detractor_service.scaling.min ]]
                max     = [[ .tnps_detractor_service.scaling.max ]]
                policy {
                    cooldown            = "[[ .tnps_detractor_service.scaling.cooldown ]]"
                    evaluation_interval = "[[ .tnps_detractor_service.scaling.evaluation_interval ]]"
                    check "cpu" {
                    source = "[[ .tnps_detractor_service.scaling_cpu.source ]]"
                    query  = "[[ .tnps_detractor_service.scaling_cpu.query ]]"
                    strategy "target-value" {
                        target = [[ .tnps_detractor_service.scaling_cpu.target ]]
                    }
                    }
                    check "memory" {
                    source = "[[ .tnps_detractor_service.scaling_memory.source ]]"
                    query  = "[[ .tnps_detractor_service.scaling_memory.query ]]"
                    strategy "target-value" {
                        target = [[ .tnps_detractor_service.scaling_memory.target ]]
                    }
                    }
                }
            }
            [[- end ]]
        }

        task "tnps_detractor_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_detractor_service.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .tnps_detractor_service.image ]]:[[ .tnps_detractor_service.image_tag ]]"
                [[- if .tnps_detractor_service.auth_image ]]
                auth {
                     [[- range $var := .tnps_detractor_service.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_detractor_service.cores_param ]]
                cores  = [[ .tnps_detractor_service.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_detractor_service.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_detractor_service.resources.memory ]]
            }
        }
    }
}
