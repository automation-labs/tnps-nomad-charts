job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .tnps_wb_service.datacenters | toStringList ]]
    namespace = [[ .tnps_wb_service.namespace | quote ]]

    [[- if .tnps_wb_service.prod_env ]]
    constraint {
        attribute = [[ .tnps_wb_service.constraint.attribute | quote ]]
        value     = [[ .tnps_wb_service.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_wb_service" {
        [[- if .tnps_wb_service.static_count ]]
        count = [[  .tnps_wb_service.tnps_wb_service_count ]]
        [[- end ]]

        network {
            mode = "bridge"
            [[- if .tnps_wb_service.expose_stanza ]]
            port "[[  .tnps_wb_service.tnps_wb_service_port ]]" {
                to = [[  .tnps_wb_service.tnps_wb_service_port ]]
                static = [[  .tnps_wb_service.tnps_wb_service_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_wb_service.service_name_tnps_wb_service ]]"
            port = "[[ .tnps_wb_service.tnps_wb_service_port ]]"
            tags = [[ .tnps_wb_service.tags | toStringList ]]
            connect {
                sidecar_service{
                }
            }

            [[- if .tnps_wb_service.enable_autocale ]]
            scaling {
                enabled = [[ .tnps_wb_service.scaling.enabled ]]
                min     = [[ .tnps_wb_service.scaling.min ]]
                max     = [[ .tnps_wb_service.scaling.max ]]
                policy {
                    cooldown            = "[[ .tnps_wb_service.scaling.cooldown ]]"
                    evaluation_interval = "[[ .tnps_wb_service.scaling.evaluation_interval ]]"
                    check "cpu" {
                    source = "[[ .tnps_wb_service.scaling_cpu.source ]]"
                    query  = "[[ .tnps_wb_service.scaling_cpu.query ]]"
                    strategy "target-value" {
                        target = [[ .tnps_wb_service.scaling_cpu.target ]]
                    }
                    }
                    check "memory" {
                    source = "[[ .tnps_wb_service.scaling_memory.source ]]"
                    query  = "[[ .tnps_wb_service.scaling_memory.query ]]"
                    strategy "target-value" {
                        target = [[ .tnps_wb_service.scaling_memory.target ]]
                    }
                    }
                }
            }
            [[- end ]]
        }

        task "tnps_wb_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_wb_service.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .tnps_wb_service.image ]]:[[ .tnps_wb_service.image_tag ]]"
                [[- if .tnps_wb_service.auth_image ]]
                auth {
                     [[- range $var := .tnps_wb_service.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_wb_service.cores_param ]]
                cores  = [[ .tnps_wb_service.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_wb_service.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_wb_service.resources.memory ]]
            }
        }
    }
}
