job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .lacima_poller_service.datacenters | toStringList ]]
    namespace = [[ .lacima_poller_service.namespace | quote ]]

    [[- if .lacima_poller_service.cons_param ]]
    constraint {
        attribute = [[ .lacima_poller_service.constraint.attribute | quote ]]
        value     = [[ .lacima_poller_service.constraint.value | quote ]]
        operator  = [[ .lacima_poller_service.constraint.operator | quote ]]
    }
    [[- end ]]

    group "lacima_poller_service" {
        [[- if .lacima_poller_service.static_count ]]
        count = [[  .lacima_poller_service.lacima_poller_service_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .lacima_poller_service.expose_stanza ]]
            port "[[  .lacima_poller_service.lacima_poller_service_port ]]" {
                to = [[  .lacima_poller_service.lacima_poller_service_port ]]
                static = [[  .lacima_poller_service.lacima_poller_service_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .lacima_poller_service.service_name_lacima_poller_service ]]"
            port = "[[ .lacima_poller_service.lacima_poller_service_port ]]"
            tags = [[ .lacima_poller_service.tags | toStringList ]]
            connect {
                sidecar_service{
                }
            }

        [[- if .lacima_poller_service.enable_autocale ]]
        scaling {
            enabled = [[ .lacima_poller_service.scaling.enabled ]]
            min     = [[ .lacima_poller_service.scaling.min ]]
            max     = [[ .lacima_poller_service.scaling.max ]]
            policy {
                cooldown            = "[[ .lacima_poller_service.scaling.cooldown ]]"
                evaluation_interval = "[[ .lacima_poller_service.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .lacima_poller_service.scaling_cpu.source ]]"
                query  = "[[ .lacima_poller_service.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .lacima_poller_service.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .lacima_poller_service.scaling_memory.source ]]"
                query  = "[[ .lacima_poller_service.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .lacima_poller_service.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "lacima_poller_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .lacima_poller_service.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .lacima_poller_service.image ]]:[[ .lacima_poller_service.image_tag ]]"
                [[- if .lacima_poller_service.auth_image ]]
                auth {
                     [[- range $var := .lacima_poller_service.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .lacima_poller_service.cores_param ]]
                cores  = [[ .lacima_poller_service.resources.cores ]]
                [[- else ]]
                cpu = [[ .lacima_poller_service.resources.cpu ]]
                [[- end ]]
                memory = [[ .lacima_poller_service.resources.memory ]]
            }
        }
    }
}
