variable "job_name" {
  # If "", the pack name will be used
  description = "The name to use as the job name which overrides using the pack name"
  type        = string
  default     = ""
}

variable "region" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement"
  type        = list(string)
  default     = ["dc1"]
}

variable "namespace" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = "lacima"
}

variable "constraint" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        operator    = string
        })
        default = {
        attribute = "nomad-worker-2"
        value     = "$${attr.unique.hostname}"
        operator  = "set_contains"
        }
}

variable "constraint_tsel" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        operator    = string
        })
        default = {
        attribute = "nomadworkertbspapp30,nomadworkertbspapp29,nomadworkertbspapp28,nomadworkertbspapp26"
        value     = "$${attr.unique.hostname}"
        operator  = "set_contains"
        }
}

variable "expose_stanza"{
  description = "enable expose stanza" 
  type = bool
  default = true
}

variable "cores_param"{
  description = "cores param" 
  type = bool
  default = false
}

variable "cons_param"{
  description = "enable contains" 
  type = bool
  default = false
}

variable "enable_autoscale"{
  description = "enable autoscale" 
  type = bool
  default = false
}

variable "static_count"{
  description = "enable count static" 
  type = bool
  default = false
}

variable "lacima_poller_service_count" {
  description = "lacima-poller-service static count, this count use for lacima-poller-service replication"
  type = number
  default = 4
}

variable "scaling" {
  description = "autoscaler"
  type = object({
    enabled = bool
    min = number
    max = number
    cooldown = string
    evaluation_interval = string
  })
  default = {
    enabled = true,
    min = 1,
    max = 5,
    cooldown = "20s",
    evaluation_interval = "10s"
  }
}

variable "scaling_cpu" {
  description = "scaling_cpu"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_cpu-allocated",
    target = 50
  }
}

variable "scaling_memory" {
  description = "scaling_memory"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_memory-allocated",
    target = 50
  }
}

variable "service_name_lacima_poller_service" {
  description = "service name lacima-poller-service"
  type        = string
  default     = "lacima-poller-service"
}

variable "lacima_poller_service_port" {
  description = "lacima-poller-service port, this port use for lacima-poller-service connection"
  type = number
  default = 9899
}

variable "tags" {
  description = "A list of tags traefik"
  type        = list(string)
  default     = [
        "traefik.enable=true",
        "traefik.http.routers.poller-service.rule=Host(`poller-service.lacima.atklik.xyz`)",
        "traefik.http.routers.poller-service.service=poller-service"
      ]
}

variable "image" {
  description = "Docker image for lacima-poller-service"
  type        = string 
  default     = "registry.digitalocean.com/labs-registry/lacima/poller-service"
}

variable "image_tag" {
  description = "Docker image tags for lacima-poller-service"
  type        = string
  default     = "alpha-26"
}

variable "auth_image" {
  description = "enable auth image" 
  type = bool
  default = true
}

variable "docker_auth" {
  description = "Docker image for lacima-poller-service"
  type = list(object({
    key   = string
    value = string
  }))
  default = [
    {key = "username", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"},
    {key = "password", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"}
  ]
}

variable "secret" {
  description = "environment secret lacima-poller-service"
  type = string
  default = <<EOF

    EOF
}

variable "certificate_rootca" {
  description = ".crt"
  type = string
  default = <<EOF
EOF
}

variable "resources" {
  description = "The resource to assign to the lacima-poller-service"
  type = object({
    cores  = number
    cpu    = number
    memory = number
  })
  default = {
    cores  = 1,
    cpu    = 300,
    memory = 500
  }
}
