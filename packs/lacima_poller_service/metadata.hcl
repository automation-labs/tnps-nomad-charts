app {
  url = "https://www.kliklabs.com/"
  
  author = "Kliklabs Team"
}

pack {
  name = "lacima_poller_service"
  description = "lacima_poller_service chart, tech stack that used by kliklabs nomad lacima"
  url = "https://gitlab.com/kliklab/usecase/lacima/nomad-chart"
  version = "0.0.1"
}