app {
  url = "https://www.kliklabs.com/"
  
  author = "Kliklabs Team"
}

pack {
  name = "tnps_user_service"
  description = "tnps_user_service chart, tech stack that used by kliklabs nomad tnps"
  url = "https://gitlab.com/kliklab/usecase/tnps/nomad-chart"
  version = "0.0.1"
}