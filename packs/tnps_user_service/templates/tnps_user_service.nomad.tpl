job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .tnps_user_service.datacenters | toStringList ]]
    namespace = [[ .tnps_user_service.namespace | quote ]]

    [[- if .tnps_user_service.prod_env ]]
    constraint {
        attribute = [[ .tnps_user_service.constraint.attribute | quote ]]
        value     = [[ .tnps_user_service.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_user_service" {
        [[- if .tnps_user_service.static_count ]]
        count = [[  .tnps_user_service.tnps_user_service_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .tnps_user_service.expose_stanza ]]
            port "[[  .tnps_user_service.tnps_user_service_port ]]" {
                to = [[  .tnps_user_service.tnps_user_service_port ]]
                static = [[  .tnps_user_service.tnps_user_service_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_user_service.service_name_tnps_user_service ]]"
            port = "[[ .tnps_user_service.tnps_user_service_port ]]"
            tags = [[ .tnps_user_service.tags | toStringList ]]
            connect {
                sidecar_service{
                }
            }

        [[- if .tnps_user_service.enable_autocale ]]
        scaling {
            enabled = [[ .tnps_user_service.scaling.enabled ]]
            min     = [[ .tnps_user_service.scaling.min ]]
            max     = [[ .tnps_user_service.scaling.max ]]
            policy {
                cooldown            = "[[ .tnps_user_service.scaling.cooldown ]]"
                evaluation_interval = "[[ .tnps_user_service.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .tnps_user_service.scaling_cpu.source ]]"
                query  = "[[ .tnps_user_service.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_user_service.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .tnps_user_service.scaling_memory.source ]]"
                query  = "[[ .tnps_user_service.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_user_service.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "tnps_user_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_user_service.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .tnps_user_service.image ]]:[[ .tnps_user_service.image_tag ]]"
                [[- if .tnps_user_service.auth_image ]]
                auth {
                     [[- range $var := .tnps_user_service.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_user_service.cores_param ]]
                cores  = [[ .tnps_user_service.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_user_service.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_user_service.resources.memory ]]
            }
        }
    }
}
