job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .lacima_web_ui.datacenters | toStringList ]]
    namespace = [[ .lacima_web_ui.namespace | quote ]]

    [[- if .lacima_web_ui.prod_env ]]
    constraint {
        attribute = [[ .lacima_web_ui.constraint.attribute | quote ]]
        value     = [[ .lacima_web_ui.constraint.value | quote ]]
    }
    [[- end ]]

    group "lacima_web_ui" {
        [[- if .lacima_web_ui.static_count ]]
        count = [[  .lacima_web_ui.lacima_web_ui_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .lacima_web_ui.expose_stanza ]]
            port "[[  .lacima_web_ui.lacima_web_ui_port ]]" {
                to = [[  .lacima_web_ui.lacima_web_ui_port ]]
                static = [[  .lacima_web_ui.lacima_web_ui_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .lacima_web_ui.service_name_lacima_web_ui ]]"
            port = "[[ .lacima_web_ui.lacima_web_ui_port ]]"
            tags = [[ .lacima_web_ui.tags | toStringList ]]
            connect {
                sidecar_service{
                    proxy {
                        upstreams {
                        destination_name = "lacima-apigw"
                        local_bind_port  = 1212
                        }
                    }
                }
            }

        [[- if .lacima_web_ui.enable_autocale ]]
        scaling {
            enabled = [[ .lacima_web_ui.scaling.enabled ]]
            min     = [[ .lacima_web_ui.scaling.min ]]
            max     = [[ .lacima_web_ui.scaling.max ]]
            policy {
                cooldown            = "[[ .lacima_web_ui.scaling.cooldown ]]"
                evaluation_interval = "[[ .lacima_web_ui.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .lacima_web_ui.scaling_cpu.source ]]"
                query  = "[[ .lacima_web_ui.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .lacima_web_ui.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .lacima_web_ui.scaling_memory.source ]]"
                query  = "[[ .lacima_web_ui.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .lacima_web_ui.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "lacima_web_ui" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .lacima_web_ui.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .lacima_web_ui.image ]]:[[ .lacima_web_ui.image_tag ]]"
                [[- if .lacima_web_ui.auth_image ]]
                auth {
                     [[- range $var := .lacima_web_ui.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .lacima_web_ui.cores_param ]]
                cores  = [[ .lacima_web_ui.resources.cores ]]
                [[- else ]]
                cpu = [[ .lacima_web_ui.resources.cpu ]]
                [[- end ]]
                memory = [[ .lacima_web_ui.resources.memory ]]
            }
        }
    }
}
