job [[ template "job_name" . ]] {
  [[ template "region" . ]]
  datacenters = [[ .tnps_apigw.datacenters | toStringList ]]
  namespace = [[ .tnps_apigw.namespace | quote ]]
  type = "service"

  [[- if .tnps_apigw.prod_env ]]
    constraint {
        attribute = [[ .tnps_apigw.constraint.attribute | quote ]]
        value     = [[ .tnps_apigw.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_apigw" {
        [[- if .tnps_apigw.static_count ]]
        count = [[  .tnps_apigw.tnps_apigw_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .tnps_apigw.expose_stanza ]]
            port "[[  .tnps_apigw.tnps_apigw_port ]]" {
                to = [[  .tnps_apigw.tnps_apigw_port ]]
                static = 28000
            }
            port "[[  .tnps_apigw.tnps_apigw_port_metrics ]]" {
                to = [[  .tnps_apigw.tnps_apigw_port_metrics ]]
                static = 29000
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_apigw.service_name_tnps_apigw ]]"
            port = "[[ .tnps_apigw.tnps_apigw_port ]]"
            tags = [[ .tnps_apigw.tags | toStringList ]]
            connect {
                sidecar_service{
            proxy {
            upstreams {
              destination_name = "tnps-user-service"
              local_bind_port  = 1313
            }
            upstreams {
              destination_name = "tnps-channel-service"
              local_bind_port  = 1414
            }
            upstreams {
              destination_name = "tnps-survey-service"
              local_bind_port  = 1515
            }
            upstreams {
              destination_name = "tnps-detractor-service"
              local_bind_port  = 1616
            }
            upstreams {
              destination_name = "tnps-wb-service"
              local_bind_port  = 1717
            }
          }
        }
        }

        [[- if .tnps_apigw.enable_autocale ]]
        scaling {
            enabled = [[ .tnps_apigw.scaling.enabled ]]
            min     = [[ .tnps_apigw.scaling.min ]]
            max     = [[ .tnps_apigw.scaling.max ]]
            policy {
                cooldown            = "[[ .tnps_apigw.scaling.cooldown ]]"
                evaluation_interval = "[[ .tnps_apigw.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .tnps_apigw.scaling_cpu.source ]]"
                query  = "[[ .tnps_apigw.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_apigw.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .tnps_apigw.scaling_memory.source ]]"
                query  = "[[ .tnps_apigw.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_apigw.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        service {
            name = "[[ .tnps_apigw.service_name_tnps_apigw_metrics ]]"
            port = "[[ .tnps_apigw.tnps_apigw_port_metrics ]]"
        }

        task "tnps_apigw" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_apigw.secret]]
                EOF

                destination = "/local/klikapim.json"
                env = false
            }

            config {
                image = "[[ .tnps_apigw.image ]]:[[ .tnps_apigw.image_tag ]]"
                mount {
                     [[- range $var := .tnps_apigw.mount ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- if .tnps_apigw.auth_image ]]
                auth {
                     [[- range $var := .tnps_apigw.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_apigw.cores_param ]]
                cores  = [[ .tnps_apigw.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_apigw.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_apigw.resources.memory ]]
            }
        }
    }
}

