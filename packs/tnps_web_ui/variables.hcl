variable "job_name" {
  # If "", the pack name will be used
  description = "The name to use as the job name which overrides using the pack name"
  type        = string
  default     = ""
}

variable "region" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement"
  type        = list(string)
  default     = ["dc1"]
}

variable "namespace" {
  description = "The region where jobs will be deployed"
  type        = string
  default     = "tnps"
}

variable "constraint" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        })
        default = {
        attribute = "$${meta.node}"
        value     = "tnps"
        }
}

variable "constraint_tsel" {
    description = "enable constraints to selected nodes"
    type = object({
        attribute   = string
        value       = string
        operator    = string
        })
        default = {
        attribute = "nomadworkertbspapp30,nomadworkertbspapp29,nomadworkertbspapp28,nomadworkertbspapp26"
        value     = "$${attr.unique.hostname}"
        operator  = "set_contains"
        }
}

variable "expose_stanza"{
  description = "enable expose stanza" 
  type = bool
  default = true
}

variable "cores_param"{
  description = "cores param" 
  type = bool
  default = false
}

variable "prod_env"{
  description = "enable contains" 
  type = bool
  default = false
}

variable "enable_autoscale"{
  description = "enable autoscale" 
  type = bool
  default = false
}

variable "static_count"{
  description = "enable count static" 
  type = bool
  default = false
}

variable "tnps_web_ui_count" {
  description = "tnps-web-ui static count, this count use for tnps-web-ui replication"
  type = number
  default = 3
}

variable "scaling" {
  description = "autoscaler"
  type = object({
    enabled = bool
    min = number
    max = number
    cooldown = string
    evaluation_interval = string
  })
  default = {
    enabled = true,
    min = 1,
    max = 5,
    cooldown = "20s",
    evaluation_interval = "10s"
  }
}

variable "scaling_cpu" {
  description = "scaling_cpu"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_cpu-allocated",
    target = 50
  }
}

variable "scaling_memory" {
  description = "scaling_memory"
  type = object({
    source = string
    query = string
    target = number
  })
  default = {
    source = "nomad-apm",
    query = "avg_memory-allocated",
    target = 50
  }
}

variable "service_name_tnps_web_ui" {
  description = "service name tnps-web-ui"
  type        = string
  default     = "tnps-web-ui"
}

variable "tnps_web_ui_port" {
  description = "tnps-web-ui port, this port use for tnps-web-ui connection"
  type = number
  default = 4400
}

variable "tags" {
  description = "A list of tags traefik"
  type        = list(string)
  default     = [
        "traefik.enable=true",
        "traefik.http.routers.web-ui.rule=Host(`tnps-web-ui.nomad.atklik.xyz`)",
        "traefik.http.routers.web-ui.service=tnps-web-ui"
      ]
}

variable "image" {
  description = "Docker image for tnps-web-ui"
  type        = string 
  default     = "registry.digitalocean.com/labs-registry/tnps/web-ui"
}

variable "image_tag" {
  description = "Docker image tags for tnps-web-ui"
  type        = string
  default     = "alpha-26"
}

variable "auth_image" {
  description = "enable auth image" 
  type = bool
  default = true
}

variable "docker_auth" {
  description = "Docker image for tnps-web-ui"
  type = list(object({
    key   = string
    value = string
  }))
  default = [
    {key = "username", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"},
    {key = "password", value = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"}
  ]
}

variable "secret" {
  description = "environment secret tnps-web-ui"
  type = string
  default = <<EOF
    
    EOF
}

variable "certificate_rootca" {
  description = ".crt"
  type = string
  default = <<EOF
EOF
}

variable "resources" {
  description = "The resource to assign to the tnps-web-ui"
  type = object({
    cores  = number
    cpu    = number
    memory = number
  })
  default = {
    cores  = 1,
    cpu    = 500,
    memory = 800
  }
}
