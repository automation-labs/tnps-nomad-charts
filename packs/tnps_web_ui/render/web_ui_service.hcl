job "tnps_web_ui" {
    
    datacenters = ["dc1"]

    namespace = "tnps"
    
    constraint {
        attribute = "${meta.node}"
        value     = "docman"
    }

    group "tnps-web-ui" {
        network {
            mode = "bridge"
            port "http" {
                to = 21123
            }
            dns {
                servers = [
  "8.8.8.8",
  "8.8.4.4"
]
                searches = [
  "8.8.8.8",
  "8.8.4.4"
]
                options = [
  "8.8.8.8",
  "8.8.4.4"
]
            }
        }

        service {
            name = "tnps_web_ui"
            port = "http"
            tags = [
            "traefik.enable=true",
            "traefik.http.routers.tnps-web-ui.rule=Host(`tnps-web-ui.tnps.atklik.xyz`)",
            "traefik.http.routers.tnps-web-ui.service=tnps-tnps-web-ui"
          ]
            connect {
                sidecar_service{
                }
            }
        }

        task "web_ui_service" {
            driver = "docker"

            template {
                data = <<EOF
                   NEXT_APP_WEBSITE_NAME=TNPS_WEBUI
                   NEXT_PUBLIC_API=https://tnps-user.labs-nonprod.atklik.xyz/api
                   NEXT_PUBLIC_API_CHANNEL=https://tnps-channel.labs-nonprod.atklik.xyz/api
                   NEXT_PUBLIC_API_IP=http://209.97.166.116:31945/kliklabs

                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "registry.digitalocean.com/labs-registry/tnps/web-ui:alpha-80"
                auth {
                    username = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"
                    password = "dop_v1_3b958aef7fb154918311dd21c2e20b1e45a819c89851b010b0cd764dfcd340bc"
                }
                ports = ["http"]
            }

            resources {
                cpu    = 300
                memory = 500
            }
        }
    }
}

