job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .tnps_web_ui.datacenters | toStringList ]]
    namespace = [[ .tnps_web_ui.namespace | quote ]]

    [[- if .tnps_web_ui.prod_env ]]
    constraint {
        attribute = [[ .tnps_web_ui.constraint.attribute | quote ]]
        value     = [[ .tnps_web_ui.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_web_ui" {
        [[- if .tnps_web_ui.static_count ]]
        count = [[  .tnps_web_ui.tnps_web_ui_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .tnps_web_ui.expose_stanza ]]
            port "[[  .tnps_web_ui.tnps_web_ui_port ]]" {
                to = [[  .tnps_web_ui.tnps_web_ui_port ]]
                static = [[  .tnps_web_ui.tnps_web_ui_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_web_ui.service_name_tnps_web_ui ]]"
            port = "[[ .tnps_web_ui.tnps_web_ui_port ]]"
            tags = [[ .tnps_web_ui.tags | toStringList ]]
            connect {
                sidecar_service{
                    proxy {
                        upstreams {
                        destination_name = "tnps-apigw"
                        local_bind_port  = 1212
                        }
                    }
                }
            }

        [[- if .tnps_web_ui.enable_autocale ]]
        scaling {
            enabled = [[ .tnps_web_ui.scaling.enabled ]]
            min     = [[ .tnps_web_ui.scaling.min ]]
            max     = [[ .tnps_web_ui.scaling.max ]]
            policy {
                cooldown            = "[[ .tnps_web_ui.scaling.cooldown ]]"
                evaluation_interval = "[[ .tnps_web_ui.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .tnps_web_ui.scaling_cpu.source ]]"
                query  = "[[ .tnps_web_ui.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_web_ui.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .tnps_web_ui.scaling_memory.source ]]"
                query  = "[[ .tnps_web_ui.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_web_ui.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "tnps_web_ui" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_web_ui.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .tnps_web_ui.image ]]:[[ .tnps_web_ui.image_tag ]]"
                [[- if .tnps_web_ui.auth_image ]]
                auth {
                     [[- range $var := .tnps_web_ui.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_web_ui.cores_param ]]
                cores  = [[ .tnps_web_ui.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_web_ui.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_web_ui.resources.memory ]]
            }
        }
    }
}
