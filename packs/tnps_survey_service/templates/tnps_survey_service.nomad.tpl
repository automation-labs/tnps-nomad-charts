job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .tnps_survey_service.datacenters | toStringList ]]
    namespace = [[ .tnps_survey_service.namespace | quote ]]

    [[- if .tnps_survey_service.prod_env ]]
    constraint {
        attribute = [[ .tnps_survey_service.constraint.attribute | quote ]]
        value     = [[ .tnps_survey_service.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_survey_service" {
        [[- if .tnps_survey_service.static_count ]]
        count = [[  .tnps_survey_service.tnps_survey_service_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .tnps_survey_service.expose_stanza ]]
            port "[[  .tnps_survey_service.tnps_survey_service_port ]]" {
                to = [[  .tnps_survey_service.tnps_survey_service_port ]]
                static = [[  .tnps_survey_service.tnps_survey_service_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_survey_service.service_name_tnps_survey_service ]]"
            port = "[[ .tnps_survey_service.tnps_survey_service_port ]]"
            tags = [[ .tnps_survey_service.tags | toStringList ]]
            connect {
                sidecar_service{
                }
            }

        [[- if .tnps_survey_service.enable_autocale ]]
        scaling {
            enabled = [[ .tnps_survey_service.scaling.enabled ]]
            min     = [[ .tnps_survey_service.scaling.min ]]
            max     = [[ .tnps_survey_service.scaling.max ]]
            policy {
                cooldown            = "[[ .tnps_survey_service.scaling.cooldown ]]"
                evaluation_interval = "[[ .tnps_survey_service.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .tnps_survey_service.scaling_cpu.source ]]"
                query  = "[[ .tnps_survey_service.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_survey_service.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .tnps_survey_service.scaling_memory.source ]]"
                query  = "[[ .tnps_survey_service.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_survey_service.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "tnps_survey_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_survey_service.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .tnps_survey_service.image ]]:[[ .tnps_survey_service.image_tag ]]"
                [[- if .tnps_survey_service.auth_image ]]
                auth {
                     [[- range $var := .tnps_survey_service.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_survey_service.cores_param ]]
                cores  = [[ .tnps_survey_service.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_survey_service.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_survey_service.resources.memory ]]
            }
        }
    }
}
