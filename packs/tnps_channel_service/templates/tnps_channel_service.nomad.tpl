job [[ template "job_name" . ]] {
    [[ template "region" . ]]
    datacenters = [[ .tnps_channel_service.datacenters | toStringList ]]
    namespace = [[ .tnps_channel_service.namespace | quote ]]

    [[- if .tnps_channel_service.prod_env ]]
    constraint {
        attribute = [[ .tnps_channel_service.constraint.attribute | quote ]]
        value     = [[ .tnps_channel_service.constraint.value | quote ]]
    }
    [[- end ]]

    group "tnps_channel_service" {
        [[- if .tnps_channel_service.static_count ]]
        count = [[  .tnps_channel_service.tnps_channel_service_count ]]
        [[- end ]]
        network {
            mode = "bridge"
            [[- if .tnps_channel_service.expose_stanza ]]
            port "[[  .tnps_channel_service.tnps_channel_service_port ]]" {
                to = [[  .tnps_channel_service.tnps_channel_service_port ]]
                static = [[  .tnps_channel_service.tnps_channel_service_port ]]
            }
            [[- end ]]
        }

        service {
            name = "[[ .tnps_channel_service.service_name_tnps_channel_service ]]"
            port = "[[ .tnps_channel_service.tnps_channel_service_port ]]"
            tags = [[ .tnps_channel_service.tags | toStringList ]]
            connect {
                sidecar_service{
                }
            }

        [[- if .tnps_channel_service.enable_autocale ]]
        scaling {
            enabled = [[ .tnps_channel_service.scaling.enabled ]]
            min     = [[ .tnps_channel_service.scaling.min ]]
            max     = [[ .tnps_channel_service.scaling.max ]]
            policy {
                cooldown            = "[[ .tnps_channel_service.scaling.cooldown ]]"
                evaluation_interval = "[[ .tnps_channel_service.scaling.evaluation_interval ]]"
                check "cpu" {
                source = "[[ .tnps_channel_service.scaling_cpu.source ]]"
                query  = "[[ .tnps_channel_service.scaling_cpu.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_channel_service.scaling_cpu.target ]]
                }
                }
                check "memory" {
                source = "[[ .tnps_channel_service.scaling_memory.source ]]"
                query  = "[[ .tnps_channel_service.scaling_memory.query ]]"
                strategy "target-value" {
                    target = [[ .tnps_channel_service.scaling_memory.target ]]
                }
                }
            }
        }
          [[- end ]]
        }

        task "tnps_channel_service" {
            driver = "docker"
            
            template {
                data = <<EOF
                [[ .tnps_channel_service.secret]]
                EOF

                destination = "/local/env"
                env = true
            }

            config {
                image = "[[ .tnps_channel_service.image ]]:[[ .tnps_channel_service.image_tag ]]"
                [[- if .tnps_channel_service.auth_image ]]
                auth {
                     [[- range $var := .tnps_channel_service.docker_auth ]]
                     [[ $var.key ]] = [[ $var.value | quote ]]
                     [[- end ]]
                }
                [[- end ]]

                ports = ["https"]
            }

            resources {
                [[- if .tnps_channel_service.cores_param ]]
                cores  = [[ .tnps_channel_service.resources.cores ]]
                [[- else ]]
                cpu = [[ .tnps_channel_service.resources.cpu ]]
                [[- end ]]
                memory = [[ .tnps_channel_service.resources.memory ]]
            }
        }
    }
}
